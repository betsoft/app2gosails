const {Client} = require('@googlemaps/google-maps-services-js');

var publicConfig = {
  key: 'AIzaSyAIotHL8ECxN_hucL66I6L3186A3Nsk0Mo',
  stagger_time: 1000, // for elevationPath
  encode_polylines: false,
  Promise: Promise,
};


module.exports = {


  async retrieveClients(req, res) {
    var clientMap = new Client({});


    var db = req.param('db');
    var rdi = sails.getDatastore('default');

    var mysql = rdi.driver.mysql;

    var connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: '',
      database: db
    });


    var client = await clients.find({
      where: {state: 1},
      select: ['id', 'ragionesociale', 'indirizzo', 'cap', 'citta', 'provincia', 'lat', 'lng']
    }).usingConnection(connection);

    var clientWOM = await clients.find({
      where: {state: 1, lat: 0, lng: 0},
      select: ['id', 'ragionesociale', 'indirizzo', 'cap', 'citta', 'provincia', 'lat', 'lng']
    }).usingConnection(connection);

    if(clientWOM.length !== 0){
      clientWOM.forEach(cliente => {

          var geocodeParams = {
            'address': cliente.indirizzo + ' ' + cliente.citta + ' ' + cliente.cap + ' ' + cliente.provincia,
            'key': 'AIzaSyAIotHL8ECxN_hucL66I6L3186A3Nsk0Mo'
          };

          // eslint-disable-next-line handle-callback-err
          clientMap.geocode({params: geocodeParams}).then(async (response) => {
            sails.log(response);
            cliente.lat = response.data.results[0].geometry.location.lat;
            cliente.lng = response.data.results[0].geometry.location.lng;
            console.log('Lati: ' + response.data.results[0].geometry.location.lat);
            console.log('Lati: ' + response.data.results[0].geometry.location.lng);

            await clients.updateOne({id: cliente.id}).set({
              lat: cliente.lat,
              lng: cliente.lng
            }).usingConnection(connection);


          }).catch(error => {sails.log(error); res.send("500");});
      });

    }

    if (client.length > 0) {
      res.status(200).json(client);
    } else {
      res.status(500).send();
    }
  },
  async addClient(req, res) {
    var db = req.param('db');
    var rdi = sails.getDatastore('default');

    var mysql = rdi.driver.mysql;

    var connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: '',
      database: db
    });
    var client = await clients.create({
      company_id: '1',
      ragionesociale: req.param('ragSoc'),
      indirizzo: req.param('indirizzo'),
      citta: req.param('citta'),
      provincia: req.param('prov'),
      cap: req.param('cap'),
      telefono: req.param('telefono'),
      mail: req.param('email'),
      piva: req.param('piva'),
      cf: req.param('cf'),
      nation_id: '106',
      state: '1'
    }).usingConnection(connection).fetch();
    if (client !== null) {
      res.status(200).send();
    } else {
      res.status(500).send();
    }
  },
};

