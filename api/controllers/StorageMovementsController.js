/**
 * StorageMovementsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var moment = require('moment');
moment().format();
module.exports = {
    async currentDeposit(req, res) {
        var db = req.param("db");
        var rdi = sails.getDatastore('default');

        var mysql = rdi.driver.mysql;

        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: db
        });
        var deposit_id = req.param("deposit");
        var queryStorage = "SELECT storage_movements.storage_id, SUM(storage_movements.quantity) as quantita, storages.descrizione, storages.prezzo as 'price' FROM storage_movements INNER JOIN storages ON storage_movements.storage_id = storages.id WHERE storage_movements.deposit_id = $1 AND storage_movements.state = 1 GROUP BY storage_id HAVING SUM(storage_movements.quantity) > 0";
        var deposit = await sails.sendNativeQuery(queryStorage, [deposit_id]).usingConnection(connection);
        sails.log(deposit.rows);
        /*var deposit = await storageMovements.find({where: {deposit_id: deposit_id, state: 1, }}).groupBy('storage_id').sum('quantity').populate('storage_id').usingConnection(connection);
        */
        if (deposit != undefined) {
            res.status(200).json(deposit.rows);
        } else {
            res.status(500).send();
        }
    },
    async insertLoad(req, res) {
        var db = req.body[0].db;
        var rdi = sails.getDatastore('default');

        var mysql = rdi.driver.mysql;

        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: db
        });

        var deposit_id = req.body[0].deposit;

        for (var key in req.body) {
            if (req.body.hasOwnProperty(key)) {
                if (key > 0) {
                    await storageMovements.create({
                        movement_tag: '',
                        typeOfGoodMovement: 'U',
                        deposit_id: 1,
                        storage_id: req.body[key].storage_id,
                        quantity: -1 * req.body[key].quantita,
                        movement_time: moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
                        description: "Scarico per carico in deposito",
                        state: 1
                    }).usingConnection(connection);

                    await storageMovements.create({
                        movement_tag: '',
                        typeOfGoodMovement: 'L',
                        deposit_id: deposit_id,
                        storage_id: req.body[key].storage_id,
                        quantity: req.body[key].quantita,
                        movement_time: moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
                        description: "Carico per deposito",
                        state: 1
                    }).usingConnection(connection);
                }
            }
        }
        res.status(200).send();
    },
    async createPDFLoad(req, res) {
        var db = req.param("db");
        var rdi = sails.getDatastore('default');

        var mysql = rdi.driver.mysql;

        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: db
        });

        var deposit = req.param("deposit");

        var setting = await settings.findOne({state: 1}).usingConnection(connection);

        var queryStorage = "SELECT storage_movements.storage_id, SUM(storage_movements.quantity) as quantita, storages.descrizione, storages.prezzo as 'price' FROM storage_movements INNER JOIN storages ON storage_movements.storage_id = storages.id WHERE storage_movements.deposit_id = $1 AND storage_movements.state = 1 GROUP BY storage_id HAVING SUM(storage_movements.quantity) > 0";
        var loadRaw = await sails.sendNativeQuery(queryStorage, [deposit]).usingConnection(connection);

        var depositParsed = loadRaw.rows;

        await sails.hooks.pdf.make("pdfLoad",
            {
                settings: setting,
                load: depositParsed,
                currentDate: moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
            },
            {
                output: 'assets/pdfs/mypdf.pdf'
            }).then(function (result) {
            console.log(result);
        }).catch(function (error) {
            console.log(error);
        });

        res.download('assets/pdfs/mypdf.pdf');
    }

};

