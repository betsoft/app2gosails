/**
 * UsersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    async login(req, res) {

        var crypto = require('crypto');
        var username = req.param('username');
        var password = req.param('password');
        var salt = "DYhG93b0qypappakakkaoUubWwvniR2G0FgaC9mi";
        var shasum = crypto.createHash('sha1');
        shasum.update(salt + password);
        var passwordCif = shasum.digest('hex');
        sails.log(passwordCif);
        var query = await users.findOne({'username': username});
        sails.log(query);
        if (query != undefined) {
            if (query.password === passwordCif) {
                var db = query.dbname;
                var id = query.id;
                var deposit = query.deposit;
                return res.status(200).json({id: id, db: db, deposit: deposit});
            } else {
                res.status(500).send();
            }
        } else {
            res.status(200).send();
        }
    },

    async recoverCode(req, res) {
        var username = req.param("username");
        var recoverCode = Math.floor(Math.random() * (999999 - 100000) + 100000);
        var dbuser = await users.findOne({where: {username: username}, select: ["username"]});
        if (dbuser != undefined) {
            if (username === dbuser.username) {

                await users.updateOne({username: username}).set({recoverCode: recoverCode});
            }
            const nodemailer = require("nodemailer");
            let transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: "mattia.piccapietra@betsoft-srl.it", // generated ethereal user
                    pass: "lbxcbkqvttduodqc", // generated ethereal password
                },
            });
            var text = "Questo è il tuo codice di recupero della password: " + recoverCode.toString();
            let info = transporter.sendMail({
                to: username,
                //to: "mattiapiccapietra@gmail.com",
                subject: "Codice Recupero Password",
                text: text,
            });

            res.status(200).send();
        }else{
            res.status(500).send();
        }
    },

    async checkCode(req, res) {
        var code = req.param("code");
        var username = req.param("id");
        var user = await users.findOne({where: {username: username, recoverCode: code}});
        if (user != undefined) {
            await users.updateOne({username: username, recoverCode: code}).set({recoverCode: null});
            res.status(200).send();
        } else {
            res.status(500).send();
        }
    },
    async changePassword(req, res) {
        var salt = "DYhG93b0qypappakakkaoUubWwvniR2G0FgaC9mi";
        var username = req.param("username");
        var password = req.param("password");
        var crypto = require('crypto');

        var shasum = crypto.createHash('sha1');
        shasum.update(salt + password);
        var passwordCif = shasum.digest('hex');

        var upd = await users.update({username: username}).set({password: passwordCif}).fetch();
        sails.log(upd);
        if (upd.id !== null)
            res.status(200).send();
        else
            res.status(500).send();
    }


};

