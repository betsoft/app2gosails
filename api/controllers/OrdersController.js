var moment = require('moment');
moment().format();
module.exports = {

    async retrieveClientOrders(req, res) {
        var db = req.param("db");
        var client_id = req.param("client");
        var rdi = sails.getDatastore('default');

        var mysql = rdi.driver.mysql;

        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: db
        });
        var order = await orders.find({
            where: {client_id: client_id},
            select: ['id', 'delivery_date', 'numero_ordine', 'state', 'completed']
        }).populate('sectional_id').usingConnection(connection);
        if (order != undefined) {
            order.forEach(function (factor, index) {
                factor.delivery_date = moment(factor.delivery_date).format("YYYY-MM-DD");
                factor.numero_ordine = '' + factor.numero_ordine + factor.sectional_id.suffix;
                factor.sectional_id = factor.sectional_id.id;
            });
            sails.log(order);
            if (order.length > 0) {
                res.status(200).json(order);
            } else {
                res.status(500).send();
            }
        } else {
            res.status(500).send();
        }
    },
    async createOrder(req, res) {

        var db = req.body[0].db;

        var client_id = req.body[0].client_id;
        var deposit_id = req.body[0].deposit;
        var tipologia = req.body[0].tipologia;
        var note = req.body[0].note;

        var array = new Array();

        var rdi = sails.getDatastore('default');

        if (tipologia == 2) {

            var deliveryData = new Date();

            var deliveryDate = moment(deliveryData).format("YYYY-MM-DD");

        } else {

            var deliveryDate = moment(req.body[0].delivery_date).format("YYYY-MM-DD");

        }

        var mysql = rdi.driver.mysql;

        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: db
        });

        var lastNumber = await sectionals.findOne({
            where: {default_order: 1},
            select: ["id", "last_number"]
        }).usingConnection(connection);
        if (lastNumber != undefined) {
            var sectionalId = lastNumber.id;
            var numero_ordine = lastNumber.last_number + 1;

            var client = await clients.findOne({
                where: {id: client_id},
                select: ['ragionesociale', 'indirizzo', 'cap', 'citta', 'provincia']
            }).usingConnection(connection);
            if (client != undefined) {
                if (tipologia == 1) {

                    var order = await orders.create({
                        numero_ordine: numero_ordine,
                        date: moment(new Date()).format("YYYY-MM-DD"),
                        delivery_date: deliveryDate,
                        note: note,
                        client_id: client_id,
                        tipologia: tipologia,
                        company_id: 1,
                        deposit_id: deposit_id,
                        sectional_id: sectionalId,
                        completed: 0,
                        client_name: client.ragionesociale,
                        client_address: client.indirizzo,
                        client_cap: client.cap,
                        client_city: client.citta,
                        client_province: client.provincia,
                        state: 1
                    }).usingConnection(connection).fetch();

                } else {

                    var order = await orders.create({
                        numero_ordine: numero_ordine,
                        date: moment(new Date()).format("YYYY-MM-DD"),
                        delivery_date: deliveryDate,
                        note: note,
                        client_id: client_id,
                        tipologia: tipologia,
                        company_id: 1,
                        deposit_id: deposit_id,
                        sectional_id: sectionalId,
                        completed: 1,
                        client_name: client.ragionesociale,
                        client_address: client.indirizzo,
                        client_cap: client.cap,
                        client_city: client.citta,
                        client_province: client.provincia,
                        state: 1
                    }).usingConnection(connection).fetch();

                }
                for (var key in req.body) {
                    if (req.body.hasOwnProperty(key)) {
                        if (key > 0) {
                            var datas = await storages.findOne({
                                where: {id: req.body[key].storage_id},
                                select: ['unit_id', 'vat_id']
                            }).usingConnection(connection);
                            sails.log(datas);
                            var row = await orderRows.create({
                                order_id: order.id,
                                oggetto: req.body[key].descrizione,
                                quantita: req.body[key].quantita,
                                prezzo: req.body[key].prezzo,
                                importo: req.body[key].importo,
                                storage_id: req.body[key].storage_id,
                                iva_id: datas.vat_id,
                                unita: datas.unit_id,
                                state: 1
                            }).usingConnection(connection).fetch();
                            sails.log(row);
                            if (tipologia == 2)
                                await storageMovements.create({
                                    typeOfGoodMovement: "U",
                                    deposit_id: deposit_id,
                                    storage_id: req.body[key].storage_id,
                                    quantity: -1 * req.body[key].quantita,
                                    movemement_number: 1,
                                    movement_time: moment(new Date()).format("YYYY-MM-DD"),
                                    description: "Scarico ordine n° " + order.numero_ordine + " del " + moment(order.delivery_date).format("YYYY-MM-DD"),
                                    state: 1
                                }).usingConnection(connection);

                        }
                    }
                }

                await sectionals.updateOne({id: order.sectional_id}).set({
                    last_number: order.numero_ordine
                }).usingConnection(connection);

                res.status(200).send();
            } else {
                res.status(500).send();
            }
        } else {
            res.status(500).send();
        }
    },
    async closeOrder(req, res) {
        var db = req.param("db");
        var rdi = sails.getDatastore('default');

        var mysql = rdi.driver.mysql;

        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: db
        });

        var deposit_id = req.param("deposit");
        var order = req.param("order");
        sails.log(order);
        var deliveryDate = moment(new Date()).format("YYYY-MM-DD");

        await orders.updateOne({id: order}).set({completed: 1}).usingConnection(connection);

        var orderNumber = await orders.findOne({
            where: {id: order},
            select: ['numero_ordine']
        }).usingConnection(connection);

        var rows = await orderRows.find({
            where: {order_id: order},
            select: ['quantita', 'storage_id']
        }).usingConnection(connection);

        rows.forEach(async function (factor, index) {
            await storageMovements.create({
                typeOfGoodMovement: "U",
                deposit_id: deposit_id,
                storage_id: factor.storage_id,
                quantity: -1 * factor.quantita,
                movemement_number: 1,
                movement_time: moment(new Date()).format("YYYY-MM-DD"),
                description: "Scarico ordine n° " + orderNumber.numero_ordine + " del " + moment(delivery_date).format("YYYY-MM-DD"),
                state: 1
            }).usingConnection(connection);
        });

        res.status(200).send();

    },
    async recreateOrder(req, res) {
        var db = req.param("db");
        var rdi = sails.getDatastore('default');

        var mysql = rdi.driver.mysql;

        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: db
        });

        var order = req.param("order");

        var delivery_date = moment(req.param("delivery_date")).format("YYYY-MM-DD");

        var order_rows = await orderRows.find({where: {state: 1, order_id: order}}).usingConnection(connection);

        var numeroOrdine = await sectionals.findOne({
            where: {default_order: 1},
            select: ['last_number']
        }).usingConnection(connection);
        numeroOrdine = numeroOrdine.last_number;
        var ordineDaReplicare = await orders.findOne({id: order}).usingConnection(connection);

        numeroOrdine++;

        await sectionals.updateOne({default_order: 1}).set({last_number: numeroOrdine}).usingConnection(connection);

        var nuovoOrdine = await orders.create({
            numero_ordine: numeroOrdine,
            date: moment(new Date()).format("YYYY-MM-DD"),
            delivery_date: delivery_date,
            note: ordineDaReplicare.note,
            client_id: ordineDaReplicare.client_id,
            tipologia: ordineDaReplicare.tipologia,
            company_id: ordineDaReplicare.company_id,
            payment_id: ordineDaReplicare.payment_id,
            deposit_id: ordineDaReplicare.deposit_id,
            sectional_id: ordineDaReplicare.sectional_id,
            bill_id: ordineDaReplicare.bill_id,
            bank_id: ordineDaReplicare.bank_id,
            completed: 0,
            client_name: ordineDaReplicare.client_name,
            client_address: ordineDaReplicare.client_address,
            client_cap: ordineDaReplicare.client_cap,
            client_city: ordineDaReplicare.client_city,
            client_province: ordineDaReplicare.client_province,
            state: 1
        }).usingConnection(connection).fetch();

        order_rows.forEach(async function (factor, index) {
            await orderRows.create({
                order_id: nuovoOrdine.id,
                oggetto: factor.oggetto,
                quantita: factor.quantita,
                prezzo: factor.prezzo,
                importo: factor.importo,
                storage_id: factor.storage_id,
                discount: factor.discount,
                company_id: factor.company_id,
                iva_id: factor.iva_id,
                unita: factor.unita,
                state: factor.state,
                line_number: factor.line_number
            }).usingConnection(connection);
        });

        res.status(200).send();

    },
    async retrieveOrderDetails(req, res) {
        var db = req.param("db");
        var rdi = sails.getDatastore('default');

        var mysql = rdi.driver.mysql;

        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: db
        });

        var order = req.param("order");

        var order_rows = await orderRows.find({
            where: {state: 1, order_id: order},
            select: ['id', 'oggetto', 'importo', 'quantita', 'prezzo', 'discount']
        }).usingConnection(connection);

        if (order_rows != undefined) {
            res.status(200).json(order_rows);
        } else {
            res.status(500).send();
        }
    },
    async retrieveTodayOrders(req, res) {
        var db = req.param("db");
        var rdi = sails.getDatastore('default');

        var mysql = rdi.driver.mysql;

        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: db
        });
        var todayOrders = await orders.find({
            where: {delivery_date: moment(new Date()).format("YYYY-MM-DD"), tipologia: 1, completed: 0, state: 1},
            select: ['id', 'delivery_date', 'numero_ordine', 'state', 'completed']
        }).populate('client_id').populate('sectional_id').usingConnection(connection);

        todayOrders.forEach(function (factor, index) {
            factor.delivery_date = moment(factor.delivery_date).format("YYYY-MM-DD");
            factor.numero_ordine = '' + factor.numero_ordine + factor.sectional_id.suffix;
            factor.sectional_id = factor.sectional_id.id;
        });

        if (todayOrders != undefined)
            res.status(200).json(todayOrders);
        else
            res.status(500).send();
    }


};

