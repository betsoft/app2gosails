module.exports = {
  tableName: 'clients',
  attributes: {
    company_id: {type: 'number', columnName: 'company_id'},
    ragionesociale: {type: 'string', columnName: 'ragionesociale'},
    indirizzo: {type: 'string', columnName: 'indirizzo'},
    cap: {type: 'string', columnName: 'cap'},
    citta: {type: 'string', columnName: 'citta'},
    provincia: {type: 'string', columnName: 'provincia'},
    telefono: {type: 'string', columnName: 'telefono'},
    fax: {type: 'string', columnName: 'fax'},
    mail: {type: 'string', columnName: 'mail'},
    piva: {type: 'string', columnName: 'piva'},
    cf: {type: 'string', columnName: 'cf'},
    nation_id: {type: 'number', columnName: 'nation_id'},
    vat_id: {type: 'number', columnName: 'vat_id'},
    state: {type: 'number', columnName: 'state'},
    discount: {type: 'number', columnName: 'discount'},
    lat: {type: 'number', columnName: 'lat'},
    lng: {type: 'number', columnName: 'lng'},
  },

};

