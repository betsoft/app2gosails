module.exports = {

    tableName: 'storage_movements',

    attributes: {
        movement_tag: {type: "string", columnName: "movement_tag"},
        typeOfGoodMovement: {type: 'string', columnName: 'type_of_good_movement'},
        company_id: {type: 'number', columnName: 'company_id', defaultsTo: 1},
        deposit_id: {type: 'number', columnName: 'deposit_id'},
        storage_id: {model: 'storages', columnName: 'storage_id'},
        quantity: {type: 'number', columnName: 'quantity'},
        movement_time: {type: 'ref', columnName: 'movement_time'},
        description: {type: 'string', columnName: 'description'},
        state: {type: 'number', columnName: 'state'}
    }
}
