module.exports = {
  tableName: 'orders',
  attributes: {
    numero_ordine: {type: 'number', columnName: 'numero_ordine'},
    date: {type: 'ref', columnName: 'date', columnType: 'datetime' },
    delivery_date: {type: 'ref', columnName: 'delivery_date', columnType: 'datetime' },
    note: {type: 'string', columnName: 'note'},
    client_id: {columnName: 'client_id', model: 'clients'},
    tipologia: {type: 'number', columnName: 'tipologia'},
    company_id: {type: 'number', columnName: 'company_id'},
    payment_id: {type: 'number', columnName: 'payment_id'},
    deposit_id: {type: 'number', columnName: 'deposit_id'},
    sectional_id: { model: 'sectionals'},
    bill_id: {type: 'number', columnName: 'bill_id', allowNull: true},
    bank_id: {type: 'number', columnName: 'bank_id'},
    completed: {type: 'number', columnName: 'completed'},
    client_name: {type: 'string', columnName: 'client_name'},
    client_address: {type: 'string', columnName: 'client_address'},
    client_cap: {type: 'number', columnName: 'client_cap'},
    client_city: {type: 'string', columnName: 'client_city'},
    client_province: {type: 'string', columnName: 'client_province'},
    state: {type: 'number', columnName: 'state'},
  },

};

