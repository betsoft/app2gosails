module.exports = {
  tableName: 'sectionals',
  attributes: {
    description: {type: 'string', columnName:'description'},
    company_id: {type: 'number', columnName:'company_id'},
    suffix: {type: 'string', columnName:'suffix', allowNull: true},
    default_order: {type: 'number', columnName:'default_order'},
    last_number: {type: 'number', columnName:'last_number'},
    state: {type: 'number', columnName:'state'},
    order_sectional: {type: 'number', columnName:'order_sectional'},

  }

}
