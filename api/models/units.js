module.exports = {

    tableName: 'units',

    attributes: {
        description: {type: 'string', columnName:'description'},
        state: {type: 'number', columnName: 'state'}
    }
}
