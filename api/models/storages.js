module.exports = {

    tableName: 'storages',

    attributes: {
        codice: {type: 'string', columnName:'codice'},
        descrizione: {type: 'string', columnName:'descrizione'},
        prezzo: {type: 'number', columnName:'qta', allowNull: true},
        unit_id: {model: 'units'},
        vat_id: {model: 'ivas'},
        state: {type: 'number', columnName:'state'}
    }
}
