/**
 * Users.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'users',
  attributes: {
    usercompany: {type: 'number', columnName: 'usercompany'},
    username: {type: 'string', columnName: 'username'},
    password: {type: 'string', columnName: 'password'},
    group_id: {type: 'number', columnName: 'group_id'},
    tare: {type: 'number', columnName: 'tare'},
    dbname: {type: 'string', columnName: 'dbname'},
    dbuser: {type: 'string', columnName: 'dbuser'},
    dbpassword: {type: 'string', columnName: 'dbpassword'},
    isLogged: {type: 'number', columnName: 'isLogged', allowNull: true},
    lastLoginDate: {type: 'ref', columnName: 'lastLoginDate'},
    isDemo: {type: 'number', columnName: 'isDemo'},
    registrationDbDate: {type: 'ref', columnName: 'registrationDbDate'},
    PayPal: {type: 'string', columnName: 'PayPal'},
    state: {type: 'number', columnName: 'state'},
    deposit: {type: 'number', columnName: 'deposit_id'},
    recoverCode: {type: 'number', columnName: 'recoverCode', allowNull: true}
  },

};

