module.exports = {

    tableName: 'order_rows',

    attributes: {
        order_id: {model: "orders"},
        oggetto: {type: 'string', columnName: 'oggetto'},
        quantita: {type: 'number', columnName: 'quantita'},
        prezzo: {type: 'number', columnName: 'prezzo'},
        importo: {type: 'number', columnName: 'importo'},
        storage_id: {type: 'number', columnName: 'storage_id'},
        discount: {type: 'number', columnName: 'discount'},
        iva_id: {model: "ivas"},
        unita: {model: "units"},
        state: {type: 'number', columnName: 'state'}
    }
}
