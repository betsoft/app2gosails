module.exports = {

    tableName: 'settings',

    attributes: {
        name: {type: 'string', columnName:'name'},
        indirizzo: {type: 'string', columnName: 'indirizzo'},
        citta: {type: 'string', columnName: 'citta'},
        prov: {type: 'string', columnName: 'prov'},
        cap: {type: 'string', columnName: 'cap'},
        tel: {type: 'string', columnName: 'tel'},
        email: {type: 'string', columnName: 'email'},
        piva: {type: 'string', columnName: 'piva'},
        state: {type: 'number', columnName: 'state'}
    }
}
