module.exports = {
    tableName: 'ivas',
    attributes: {
        percentuale: {type: 'number', columnName:'percentuale'},
        descrizione: {type: 'string', columnName:'descrizione'},
        state: {type: 'number', columnName:'state'},
    }

}
