/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },
  'POST /login': { action: 'users/login'},
  'POST /recoverCode': { action: 'users/recoverCode'},
  'POST /changePassword': { action: 'users/changePassword'},
  'POST /checkCode': { action: 'users/checkCode'},
  'POST /retrieveClients': { action: 'clients/retrieveClients'},
  'POST /addClient': { action: 'clients/addClient'},
  'POST /retrieveClientOrders': { action: 'orders/retrieveClientOrders'},
  'POST /createOrder': { action: 'orders/createOrder'},
  'POST /closeOrder': { action: 'orders/closeOrder'},
  'POST /recreateOrder': { action: 'orders/recreateOrder'},
  'POST /retrieveOrderDetails': { action: 'orders/retrieveOrderDetails'},
  'POST /retrieveTodayOrders': { action: 'orders/retrieveTodayOrders'},
  'POST /currentDeposit': { action: 'storageMovements/currentDeposit'},
  'POST /insertLoad': { action: 'storageMovements/insertLoad'},
  'POST /createPDFLoad': { action: 'storageMovements/createPDFLoad'},




  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
